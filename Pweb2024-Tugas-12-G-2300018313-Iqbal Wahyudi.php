<!-- Mendeklarasikan & Menampilkan Array -->
 <?php
 $arrsayur=array("Bayam","Kangkung","Selada","Kemangi");
 echo $arrsayur[0]. "<br>"; //Bayam
 echo $arrsayur[3]. "<br><br>"; //Kemangi

 $arrikan=array();
 $arrikan[]="Nila";
 $arrikan[]="Gurame";
 $arrikan[]="Gabus";
 $arrikan[]="Lele";
 echo $arrikan[0]. "<br>"; //Nila
 echo $arrikan[2]. "<br><br>"; //Gabus
 ?>

<!-- Array Assosiatif -->
 <?php
 $arrberat=array("Adit"=>60,"Bila"=>50,"Citra"=>65,"Dimas"=>70);
 echo $arrberat['Adit']. "<br>"; //60
 echo $arrberat['Bila']. "<br><br>"; //90

 $arrberat=array();
 $arrberat['Eka']=40;
 $arrberat['Fajar']=55;
 $arrberat['Gita']=47;
 echo $arrberat['Fajar']. "<br>"; //55
 echo $arrberat['Eka']. "<br><br>"; //40
 ?>

<!-- Array FOR & FOREACH -->
 <?php
 $arrwarna = array("Red","Orange","Yellow","Green","Blue","Purple");

 echo "<br>Menampilkan isi array dengan FOR: <br>";
 for ($i = 0; $i < count($arrwarna); $i++) {
    echo "Warna pelangi <font color=$arrwarna[$i]>" .$arrwarna[$i]."</font><br>";
 }

 echo "<br>Menampilkan isi array dengan FOREACH: <br>";
 foreach($arrwarna as $warna){
    echo "Warna pelangi <font color=$warna>" . $warna . "</font><br>";
}
 ?>

<!-- Array Asosiatif FOREACH & WHILE-LIST -->
 <?php
 $arrberat=array("Adit"=>60,"Bila"=>50,"Citra"=>65,"Dimas"=>70);
 echo "<br>Menampilkan isi array asosiatif dengan foreach: <br>";
 foreach($arrberat as $nama=>$berat){
    echo "Berat $nama=$berat<br>";
 }

 $arraynew = new ArrayIterator($arrberat);
 echo "<br>Menampilkan isi array asosiatif dengan WHILE: <br>";
 while ($arraynew->valid()) {
    $nama = $arraynew->key();
    $berat = $arraynew->current();
    echo "Berat $nama=$berat<br>";
    $arraynew->next();
}
 ?>

<!-- Struktur Array -->
 <?php
 $arrsayur=array("Bayam","Kangkung","Selada","Kemangi","brokoli");
 $arrberat=array("Adit"=>60,"Bila"=>50,"Citra"=>65,"Dimas"=>70);
 echo "<br><pre>";
 print_r($arrsayur);
 echo "<br>";
 print_r($arrberat);
 echo "</pre>";
 ?>

<!-- Array sort() & rsort() -->
 <?php
 $arrberat=array("Adit"=>60,"Bila"=>50,"Citra"=>65,"Dimas"=>70);
 echo "<br><b>Array sebelum diurutkan</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";

 sort($arrberat);
 reset($arrberat);
 echo "<b>Array setelah diurutkan dengan sort()</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";

 rsort($arrberat);
 reset($arrberat);
 echo "<b>Array setelah diurutkan dengan rsort()</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";
 ?>

<!-- Array asort() & arsort() -->
<?php
 $arrberat=array("Adit"=>60,"Bila"=>50,"Citra"=>65,"Dimas"=>70);
 echo "<br><b>Array sebelum diurutkan</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";

 asort($arrberat);
 reset($arrberat);
 echo "<b>Array setelah diurutkan dengan asort()</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";

 arsort($arrberat);
 reset($arrberat);
 echo "<b>Array setelah diurutkan dengan arsort()</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";
 ?>

<!-- Array ksort() & krsort() -->
<?php
 $arrberat=array("Adit"=>60,"Bila"=>50,"Citra"=>65,"Dimas"=>70);
 echo "<br><b>Array sebelum diurutkan</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";

 ksort($arrberat);
 reset($arrberat);
 echo "<b>Array setelah diurutkan dengan ksort()</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";

 krsort($arrberat);
 reset($arrberat);
 echo "<b>Array setelah diurutkan dengan krsort()</b>";
 echo "<pre>";
 print_r($arrberat);
 echo "</pre>";
 ?>

<!-- Pointer -->
 <?php
 $game=array('ML','PUBG','HOK','FF');
 echo "<pre>";
 print_r($game);
 echo "</pre>";
 $mode=current($game);
 echo $mode. "<br>";
 $mode=next($game);
 echo $mode. "<br>";
 $mode=current($game);
 echo $mode. "<br>";
 $mode=prev($game);
 echo $mode. "<br>";
 $mode=end($game);
 echo $mode. "<br>";
 $mode=current($game);
 echo $mode. "<br>";
 ?>

<!-- Elemen Array -->
 <?php
 $tas=array("Handphone","Dompet","Buku","Laptop","Jam tangan");
 if(in_array("Laptop",$tas)){
    echo "<br>Terdapat laptop di dalam tas itu!";
 }else{
    echo "<br>Tidak ada laptop di tas itu";
 }
 ?>

<!-- Fungsi tanpa return value & parameter -->
 <?php
 function cetak_ganjil(){
   echo "<br><br>";
   for($i = 0; $i < 50; $i++){
      if($i%2==1){
         echo "$i, ";
      }
   }
 }
 cetak_ganjil();
 ?>

<!-- Fungsi tanpa return value tapi dengan parameter -->
 <?php
 echo "<br><br>";
 function cetak_ganjil1($awal, $akhir){
   for($i = $awal; $i < $akhir; $i++){
      if($i%2==1){
         echo "$i, ";
      }
   }
 }
 $a=5;
 $b=25;
 echo "<b>Bilangan ganjil dari $a sampai $b, adalah:</b><br>";
 cetak_ganjil1($a, $b);
 ?>

<!-- Fungsi return value & parameter -->
 <?php
 echo "<br><br>";
 function luas_lingkaran($jari){
   return 3.14*$jari*$jari;
 }

 $r=15;
 echo "Luas lingkaran dengan jari-jari $r = ";
 echo luas_lingkaran($r);
 ?>

<!-- Passing by value -->
 <?php
 echo "<br><br>";
 function tambah_string($str){
   $str=$str. ", Yogyakarta";
   return $str;
 }

 $string="Universitas Ahmad Dahlan";
 echo "\$string = $string<br>";
 echo tambah_string($string). "<br>";
 echo "\$string = $string<br>";
 ?>

<!-- Passing by reference -->
 <?php
 echo "<br><br>";
 function tambah_string1($str){
   $str=$str. ", Yogyakarta";
   return $str;
 }

 $string="Universitas Ahmad Dahlan";
 echo "\$string = $string<br>";
 echo tambah_string1($string). "<br>";
 echo "\$string = $string<br>";
 ?>

<!-- UDF dan Fungsi -->
 <?php
 echo "<br><br>";
 function luas_lingkaran1($jari){
   return 3.14*$jari*$jari;
 }

 $arr=get_defined_functions();
 echo "<pre>";
 print_r($arr);
 echo "</pre>";
 ?>

<!-- Cek Keberadaan Fungsi Di Versi PHP Saat Ini -->
<?php
echo "<br><br>";
    function luas_lingkaran2($jari){
        return 3.14*$jari*$jari;
    }

    $arr = get_defined_functions();
    
    // cek keberadaan fungsi UDF
    foreach($arr['user'] as $fungsi){

        if($fungsi == 'cetakgenap'){
            echo "Fungsi cetakgenap ada di PHP versi ini";
            break;
        }else{
            echo "Fungsi cetakgenap tidak ada";
            break;
        }
    }

    // cek keberadaan fungsi bawaan
    if (function_exists('exif_read_data')){
        echo "<br><br>Fungsi exif_read_data ada di PHP versi ini";

    }else{
        echo "<br><br>Fungsi exif_reada_data tidak ada";
 
    }
?>